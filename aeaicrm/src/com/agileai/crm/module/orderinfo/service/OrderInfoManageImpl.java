package com.agileai.crm.module.orderinfo.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.agileai.crm.cxmodule.OrderInfoManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.MasterSubServiceImpl;
import com.agileai.util.StringUtil;

public class OrderInfoManageImpl extends MasterSubServiceImpl implements
		OrderInfoManage {
	public OrderInfoManageImpl() {
		super();
	}

	public String[] getTableIds() {
		List<String> temp = new ArrayList<String>();

		temp.add("_base");
		temp.add("OrderEntry");

		return temp.toArray(new String[] {});
	}

	public void createSubRecord(String subId, DataParam param) {
		String curTableName = subTableIdNameMapping.get(subId);
		processDataType(param, curTableName);
		processPrimaryKeys(curTableName, param);

		String statementId = sqlNameSpace + "." + "insert"
				+ StringUtil.upperFirst(subId) + "Record";
		this.daoHelper.insertRecord(statementId, param);
	}

	public void computeTotalMoney(String masterRecordId, String entryId) {
		String statementId = sqlNameSpace + "." + "getMasterRecord";
		DataParam param = new DataParam("ORDER_ID", masterRecordId);
		DataRow masterRecord = this.daoHelper.getRecord(statementId, param);

		BigDecimal order_delivery_cost = (BigDecimal) masterRecord
				.get("ORDER_DELIVERY_COST");
		if (order_delivery_cost == null) {
			order_delivery_cost = new BigDecimal("0.00");
		}
		statementId = sqlNameSpace + "." + "getTotalGatherRecord";
		DataParam param1 = new DataParam("ENTRY_ID", entryId);
		DataRow getRecord = this.daoHelper.getRecord(statementId, param1);
		
		statementId = sqlNameSpace + "." + "getRealPriceGatherRecord";
		DataParam param2 = new DataParam("ORDER_ID", masterRecordId);
		DataRow getEntryRecord = this.daoHelper.getRecord(statementId, param2);
		BigDecimal gather_real_price = (BigDecimal)getEntryRecord
				.get("GATHER_REAL_PRICE");
		if (gather_real_price == null) {
			gather_real_price = new BigDecimal("0.00");
		}
if(getRecord!=null){
		BigDecimal entry_number = (BigDecimal) getRecord.get("ENTRY_NUMBER");
		if (entry_number == null) {
			entry_number = new BigDecimal("0.00");
		}
		BigDecimal entry_unit_price = (BigDecimal) getRecord
				.get("ENTRY_UNIT_PRICE");
		if (entry_unit_price == null) {
			entry_unit_price = new BigDecimal("0.00");
		}
		BigDecimal entry_discount = (BigDecimal) getRecord
				.get("ENTRY_DISCOUNT");
		if (entry_discount == null) {
			entry_discount = new BigDecimal("0.00");
		}

		BigDecimal entry_real_price = entry_number.multiply(entry_unit_price)
				.multiply(entry_discount);
		
		statementId = sqlNameSpace + "." + "updateEntryRealPriceRecord";
		DataParam updateParam = new DataParam();
		updateParam.put("ENTRY_ID",entryId,"ENTRY_REAL_PRICE",entry_real_price);
		this.daoHelper.updateRecord(statementId, updateParam);
		
		statementId = sqlNameSpace + "." + "getRealPriceGatherRecord";
		DataParam realPriceparam = new DataParam("ORDER_ID", masterRecordId);
		DataRow realPriceRecord = this.daoHelper.getRecord(statementId, realPriceparam);
		BigDecimal gather_real_price1 = (BigDecimal) realPriceRecord
				.get("GATHER_REAL_PRICE");
		if (gather_real_price1 == null) {
			gather_real_price1 = new BigDecimal("0.00");
		}
		BigDecimal order_cost = order_delivery_cost.add(gather_real_price1);
		statementId = sqlNameSpace + "." + "updateMasterGatherRecord";
		DataParam updateParam1 = new DataParam();
		updateParam1.put("ORDER_ID", masterRecordId, "ORDER_COST",order_cost);
		this.daoHelper.updateRecord(statementId, updateParam1);
}else{
	BigDecimal order_cost = order_delivery_cost.add(gather_real_price);
	statementId = sqlNameSpace + "." + "updateMasterGatherRecord";
	DataParam updateParam1 = new DataParam();
	updateParam1.put("ORDER_ID", masterRecordId, "ORDER_COST",order_cost);
	this.daoHelper.updateRecord(statementId, updateParam1);
}
		
	}

	@Override
	public void changeStateRecord(DataParam param) {
		String statementId = sqlNameSpace + "." + "changeStateRecord";
		processDataType(param, tableName);
		this.daoHelper.updateRecord(statementId, param);
	}
}
